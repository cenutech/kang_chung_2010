reference_length = 0.072;

length = 1.;

diameter = 0.032/reference_length;

radius = diameter/2.;

inner_radius = 0.9*radius;

central_offset = radius/3.;

reference_point = newp;

Point (reference_point) = { 0., 0., 0., 1. };

central_point = { newp, newp + 1, newp + 2, newp + 3 };
Point (central_point[0]) = { inner_radius - central_offset, 0., 0., 1.  };
Point (central_point[1]) = { 0., inner_radius - central_offset, 0., 1.  };
Point (central_point[2]) = { -(inner_radius - central_offset), 0., 0., 1.  };
Point (central_point[3]) = { 0., -(inner_radius - central_offset), 0., 1.  };

inner_point = { newp, newp + 1, newp + 2, newp + 3 };
Point (inner_point[0]) = { inner_radius, 0., 0., 1. };
Point (inner_point[1]) = { 0., inner_radius, 0., 1. };
Point (inner_point[2]) = { -inner_radius, 0., 0., 1. };
Point (inner_point[3]) = { 0., -inner_radius, 0., 1. };

pipe_point = { newp, newp + 1, newp + 2, newp + 3 };
Point (pipe_point[0]) = { radius, 0., 0., 1. };
Point (pipe_point[1]) = { 0., radius, 0., 1. };
Point (pipe_point[2]) = { -radius, 0., 0., 1. };
Point (pipe_point[3]) = { 0., -radius, 0., 1. };

central_curve = { newc, newc + 1, newc + 2, newc + 3 };
Line (central_curve[0]) = { central_point[0], central_point[1] };
Line (central_curve[1]) = { central_point[1], central_point[2] };
Line (central_curve[2]) = { central_point[2], central_point[3] };
Line (central_curve[3]) = { central_point[3], central_point[0] };

inner_curve = { newc, newc + 1, newc + 2, newc + 3 };
Circle (inner_curve[0]) = { inner_point[0], reference_point, inner_point[1] };
Circle (inner_curve[1]) = { inner_point[1], reference_point, inner_point[2] };
Circle (inner_curve[2]) = { inner_point[2], reference_point, inner_point[3] };
Circle (inner_curve[3]) = { inner_point[3], reference_point, inner_point[0] };

pipe_curve = { newc, newc + 1, newc + 2, newc + 3 };
Circle (pipe_curve[0]) = { pipe_point[0], reference_point, pipe_point[1] };
Circle (pipe_curve[1]) = { pipe_point[1], reference_point, pipe_point[2] };
Circle (pipe_curve[2]) = { pipe_point[2], reference_point, pipe_point[3] };
Circle (pipe_curve[3]) = { pipe_point[3], reference_point, pipe_point[0] };

guiding_curve = {
    newc, newc + 1, newc + 2, newc + 3, newc + 4, newc + 5, newc + 6, newc + 7
};

Line (guiding_curve[0]) = { central_point[0], inner_point[0] };
Line (guiding_curve[1]) = { central_point[1], inner_point[1] };
Line (guiding_curve[2]) = { central_point[2], inner_point[2] };
Line (guiding_curve[3]) = { central_point[3], inner_point[3] };
Line (guiding_curve[4]) = { inner_point[0], pipe_point[0] };
Line (guiding_curve[5]) = { inner_point[1], pipe_point[1] };
Line (guiding_curve[6]) = { inner_point[2], pipe_point[2] };
Line (guiding_curve[7]) = { inner_point[3], pipe_point[3] };

central_curve_loop = newcl;

Curve Loop (central_curve_loop) = {
    central_curve[0], central_curve[1], central_curve[2], central_curve[3]
};

central_surface = news;

Plane Surface (central_surface) = { central_curve_loop };

inner_curve_loop = { newcl, newcl + 1, newcl + 2, newcl + 3 };

Curve Loop (inner_curve_loop[0]) = {
    guiding_curve[0], inner_curve[0], -guiding_curve[1], -central_curve[0]
};

Curve Loop (inner_curve_loop[1]) = {
    guiding_curve[1], inner_curve[1], -guiding_curve[2], -central_curve[1]
};

Curve Loop (inner_curve_loop[2]) = {
    guiding_curve[2], inner_curve[2], -guiding_curve[3], -central_curve[2]
};

Curve Loop (inner_curve_loop[3]) = {
    guiding_curve[3], inner_curve[3], -guiding_curve[0], -central_curve[3]
};

inner_surface = { news, news + 1, news + 2, news + 3 };
Plane Surface (inner_surface[0]) = { inner_curve_loop[0] };
Plane Surface (inner_surface[1]) = { inner_curve_loop[1] };
Plane Surface (inner_surface[2]) = { inner_curve_loop[2] };
Plane Surface (inner_surface[3]) = { inner_curve_loop[3] };

pipe_curve_loop = { newcl, newcl + 1, newcl + 2, newcl + 3 };

Curve Loop (pipe_curve_loop[0]) = {
    guiding_curve[4], pipe_curve[0], -guiding_curve[5], -inner_curve[0]
};

Curve Loop (pipe_curve_loop[1]) = {
    guiding_curve[5], pipe_curve[1], -guiding_curve[6], -inner_curve[1]
};

Curve Loop (pipe_curve_loop[2]) = {
    guiding_curve[6], pipe_curve[2], -guiding_curve[7], -inner_curve[2]
};

Curve Loop (pipe_curve_loop[3]) = {
    guiding_curve[7], pipe_curve[3], -guiding_curve[4], -inner_curve[3]
};

pipe_surface = { news, news + 1, news + 2, news + 3 };
Plane Surface (pipe_surface[0]) = { pipe_curve_loop[0] };
Plane Surface (pipe_surface[1]) = { pipe_curve_loop[1] };
Plane Surface (pipe_surface[2]) = { pipe_curve_loop[2] };
Plane Surface (pipe_surface[3]) = { pipe_curve_loop[3] };

mesh3D[] = Extrude { 0., 0., length } {
    Surface {
        central_surface,
        inner_surface[0],
        inner_surface[1],
        inner_surface[2],
        inner_surface[3],
        pipe_surface[0],
        pipe_surface[1],
        pipe_surface[2],
        pipe_surface[3]
    };
};

mesh3D[] = Extrude { 0., 0., length } {
    Surface { 60, 82, 104, 126, 148, 170, 192, 214, 236 };
};

mesh3D[] = Extrude { 0., 0., 4.*length } {
  Surface{ 258, 280, 302, 324, 346, 368, 390, 412, 434 };
};

Transfinite Line {
    central_curve[0],
    central_curve[1],
    central_curve[2],
    central_curve[3],
    inner_curve[0],
    inner_curve[1],
    inner_curve[2],
    inner_curve[3],
    pipe_curve[0],
    pipe_curve[1],
    pipe_curve[2],
    pipe_curve[3],
    40:43:1,
    63:129:22,
    151:217:22,
    238:241:1,
    261:327:22,
    349:415:22,
    436:439:1,
    459:525:22,
    547:613:22
} = 8;

Transfinite Line {
    guiding_curve[0],
    guiding_curve[1],
    guiding_curve[2],
    guiding_curve[3],
    62, -64, -86, -108,
    260, -262, -284, -306,
    458, -460, -482, -504
} = 6 Using Progression 0.8;

Transfinite Line {
    guiding_curve[4],
    guiding_curve[5],
    guiding_curve[6],
    guiding_curve[7],
    150, -152, -174, -196,
    348, -350, -372, -394,
    546, -548, -570, -592
} = 4 Using Progression 0.8;

Transfinite Line {
    45, 46, 50, 54, 68, 72, 94, 116, 156, 160, 182, 204
} = 7 Using Progression 0.57;

Transfinite Curve {
    243, 244, 248, 252, 266, 270, 292, 314, 354, 358, 380, 402
} = 11 Using Bump 0.1;

Transfinite Line {
    441, 442, 446, 450, 464, 468, 490, 512, 552, 556, 578, 600
//} = 11 Using Bump 0.015;
} = 11 Using Progression 1.5;

Recombine Surface "*";
Transfinite Surface "*";

Physical Surface ("heated_wall") = { 359, 381, 403, 425 };
Physical Surface ("wall") = {
    22, 27, 28, 29, 30, 35, 36, 37, 38, 161, 183, 205, 227, 456, 478, 500, 522,
    544, 557, 566, 579, 588, 601, 610, 623, 632
};
Physical Volume ("fluid") = { 1:27:1 };

Recombine Volume "*";
Transfinite Volume "*";

Coherence;
Mesh.MshFileVersion = 2.2;
