void scalarDirichletConditions(bcData *bc)
{
  bc->s = 1.0;
}

@kernel
void boussinesqBuoyancy(const dlong Ntotal, const int offset, @restrict dfloat * S, @restrict dfloat * source)
{
  for(dlong n = 0; n < Ntotal; ++n; @tile(p_blockSize,@outer,@inner))
  {
    if (n < Ntotal)
    {
      source[n + 0*offset] = 0.0;
      source[n + 1*offset] = 0.0;
      source[n + 2*offset] = S[n];
    }
  }
}
