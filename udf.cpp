#include "udf.hpp"

static
occa::kernel boussinesqBuoyancyKernel;

void boussinesqBuoyancy(nrs_t * nrs, dfloat time, occa::memory o_U, occa::memory o_FU);

void UDF_LoadKernels(occa::properties& kernelInfo)
{
  boussinesqBuoyancyKernel = udfBuildKernel(kernelInfo, "boussinesqBuoyancy");
}

void UDF_Setup(nrs_t *nrs)
{
  udf.uEqnSource = &boussinesqBuoyancy;

  const bool is_restart = platform->options.compareArgs("RESTART FROM FILE", "1");

  if (is_restart) return;

  for (int n = 0; n < nrs->meshV->Nlocal; ++n)
  {
    nrs->U[n + 0*nrs->fieldOffset] = 0.0;
    nrs->U[n + 1*nrs->fieldOffset] = 0.0;
    nrs->U[n + 2*nrs->fieldOffset] = 0.0;

    nrs->P[n] = 91.86;
  }

  for (int n = 0; n < nrs->cds->meshV->Nlocal; ++n)
  {
    nrs->cds->S[n] = 0.0;
  }
}

void UDF_Setup0(MPI_Comm comm, setupAide &options)
{
}

void UDF_ExecuteStep(nrs_t *nrs, dfloat time, int tstep)
{
}

void boussinesqBuoyancy(nrs_t * nrs, dfloat time, occa::memory o_U, occa::memory o_FU)
{
  boussinesqBuoyancyKernel(nrs->meshV->Nlocal, nrs->fieldOffset, nrs->cds->o_S, o_FU);
}
